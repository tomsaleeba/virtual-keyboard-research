window.addEventListener('load', function() {
  const fakeInput = document.getElementById('fake-input')
  const keyboard = document.getElementById('keyboard')

  const rows = [
    ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
    ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';'],
    ['z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'],
  ]
  for (let currRow of rows) {
    const rowEl = document.createElement('div')
    rowEl.classList = 'keeb-row'
    for (let currKey of currRow) {
      const keyEl = document.createElement('div')
      keyEl.classList = 'key'
      keyEl.innerHTML = currKey
      rowEl.appendChild(keyEl)
    }
    keyboard.appendChild(rowEl)
  }

  function sendKeystroke(key) {
    fakeInput.innerHTML += key
  }

  function backspace() {
    const oldVal = fakeInput.innerHTML
    fakeInput.innerHTML = (oldVal || '').substring(0, oldVal.length - 1)
  }

  [...document.getElementsByClassName('key')].forEach(curr => {
    const keyLetter = curr.innerHTML
    curr.addEventListener('click', () => {
      console.debug('[key clicked] ', keyLetter)
      sendKeystroke(keyLetter)
    })

    // curr.addEventListener('touchend', () => {
    //   console.log('gesture start on key', keyLetter)
    // })
  })

	var start = {};
	var end = {};
	var tracking = false;
	var thresholdTime = 500;
	var thresholdDistance = 100;
	// var o = document.getElementsByTagName('output')[0];
	gestureStart = function(e) {
		// o.innerHTML = '';
		if (e.touches.length>1) {
			tracking = false;
			return;
		} else {
			tracking = true;
			/* Hack - would normally use e.timeStamp but it's whack in Fx/Android */
			start.t = new Date().getTime();
			start.x = e.targetTouches[0].clientX;
			start.y = e.targetTouches[0].clientY;
		}
		console.dir(start);
	};
	gestureMove = function(e) {
		if (tracking) {
			e.preventDefault();
			end.x = e.targetTouches[0].clientX;
			end.y = e.targetTouches[0].clientY;
		}
	}
	gestureEnd = function(e) {
		tracking = false;
		var now = new Date().getTime();
		var deltaTime = now - start.t;
		var deltaX = end.x - start.x;
		var deltaY = end.y - start.y;
		/* work out what the movement was */
		if (deltaTime > thresholdTime) {
			/* gesture too slow */
			return;
		}
    if ((deltaX > thresholdDistance)&&(Math.abs(deltaY) < thresholdDistance)) {
      console.log('swipe right')
      sendKeystroke(' ')
    } else if ((-deltaX > thresholdDistance)&&(Math.abs(deltaY) < thresholdDistance)) {
      console.log('swipe left')
      backspace()
    } else if ((deltaY > thresholdDistance)&&(Math.abs(deltaX) < thresholdDistance)) {
      console.log('swipe down')
    } else if ((-deltaY > thresholdDistance)&&(Math.abs(deltaX) < thresholdDistance)) {
      console.log('swipe up')
    } else {
      console.log('confused swipe') // FIXME
    }
	}

	keyboard.addEventListener('touchstart', gestureStart, false);
	keyboard.addEventListener('touchmove', gestureMove, false);
	keyboard.addEventListener('touchend', gestureEnd, false);

}, false);
