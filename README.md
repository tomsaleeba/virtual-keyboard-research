TODO:
- block unintended behaviour like selecting text or zooming
- swipe logic not blocking regular tap logic

# Demo
https://tomsaleeba.gitlab.io/virtual-keyboard-research/

# Research

## Vibration
It's supported in Chrome and we can get sufficiently short durations.
https://sbedell.github.io/vibrate
https://developer.mozilla.org/en-US/docs/Web/API/Vibration_API

## Tap swipes, with direction
Yes, it's supported.
https://github.com/patrickhlauke/touch/blob/gh-pages/swipe/index.html
